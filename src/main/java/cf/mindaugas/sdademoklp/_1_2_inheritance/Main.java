package cf.mindaugas.sdademoklp._1_2_inheritance;

public class Main {
    public static void main(String[] args) {
        Nurse n1 = new Nurse("Mindaugas", "XXL");
        n1.setName("Mindaugas");

        System.out.println(n1);
    }
}

class Employee {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Nurse extends Employee {
    private String foodTrolley;
    public Nurse(String name, String foodTrolley) {
        super(name);
        this.foodTrolley = foodTrolley;
    }

    @Override
    public String toString() {
        return "Nurse{" + "foodTrolley='" + foodTrolley + '\'' +"} " + super.toString();
    }

    // public String toString(){
    //     return "{'name':" + this.getName() + ", 'foodTrolley':" + this.foodTrolley + "}";
    // }
}

class Doctor extends Employee {
    public Doctor(String name) {
        super(name);
    }
}
