package cf.mindaugas.sdademoklp._3_3_concurrency;

public class Main {
    public static void main(String[] args) throws InterruptedException{
        //1. Simple thread
        StopWatchThread stopWatchThread = new StopWatchThread();
        stopWatchThread.start();

        //0.  Thread sleep
        System.out.println("Main thread starts");
        Thread.sleep(3000);
        System.out.println("Main thread is still running");
        Thread.sleep(3000);
        System.out.println("Main thread ends");
    }
}

class StopWatchThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Stop watch: " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
