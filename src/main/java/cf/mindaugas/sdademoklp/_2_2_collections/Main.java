package cf.mindaugas.sdademoklp._2_2_collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        // creating
        // K - Integer
        // V - Person
        Map<Integer, Person> ssnToPeopleMap = new HashMap<>();

        // adding values
        ssnToPeopleMap.put(465454651, new Person("Jonas", "Jonaitis", 15));
        ssnToPeopleMap.put(878949884, new Person("Petras", "Petraitis", 20));

        // iterating over a map
        for(Map.Entry<Integer, Person> dictionary: ssnToPeopleMap.entrySet()) {
            Integer ssn = dictionary.getKey();
            Person person = dictionary.getValue();
            System.out.printf("%s : %s\n", ssn, person);
        }
        System.out.println("-----------------------");

        // Removal
        System.out.println(ssnToPeopleMap.remove(878949884));

        for(Map.Entry<Integer, Person> dictionary: ssnToPeopleMap.entrySet()) {
            Integer ssn = dictionary.getKey();
            Person person = dictionary.getValue();
            System.out.printf("%s : %s\n", ssn, person);
        }

        // print hashcode
        System.out.println((new Person("Jonas", "Jonaitis", 15)).hashCode());
        System.out.println((new Person("Jonas", "Jonaitis", 15)).hashCode());


        // Why do we need hashCode()
        Set<Person> personSet = new HashSet<Person>();
        personSet.add(new Person("Jonas", "Jonaitis", 15));
        personSet.add(new Person("Jonas", "Jonaitis", 15));

        System.out.println("----------- TODO DEMO -----------");
        for(Person p : personSet)
            System.out.println(p);
    }
}

class Person {
    private String firstName;
    private String lastName;
    private int age;

    public Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public int getAge(){
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;
    }

    @Override
    public int hashCode() {
        // int result = firstName != null ? firstName.hashCode() : 0;
        // result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        // result = 31 * result + age;
        // return result;
        return 1;
    }
}
