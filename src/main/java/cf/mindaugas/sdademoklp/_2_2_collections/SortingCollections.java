package cf.mindaugas.sdademoklp._2_2_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingCollections {
    public static void main(String[] args) {
        // Sorting a collection
        List<Product> products = new ArrayList<>();
        products.add(new Product("Magnetola", 150));
        products.add(new Product("Videkas", 1500));
        products.add(new Product("CD Grotuvas", 200));

        System.out.println("List BEFORE the use of Collection.sort() : " + products);
        Collections.sort(products);
        System.out.println("List AFTER the use of Collection.sort() : " + products);
    }
}

class Product implements Comparable<Product> {

    String name;
    int weight;

    Product(String name, int weight){
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "{" + name  + ":" + weight + '}';
    }

    // @Override
    // public int compareTo(Object o) {
    //     if(this.weight == ((Product)o).weight)
    //         return 0;
    //     else if (this.weight > ((Product)o).weight)
    //         return 1;
    //     else
    //         return -1;
    // }

    @Override
    public int compareTo(Product product) {
        if(this.weight == product.weight)
            return 0;
        else if (this.weight > product.weight)
            return 1;
        else
            return -1;
    }
}

