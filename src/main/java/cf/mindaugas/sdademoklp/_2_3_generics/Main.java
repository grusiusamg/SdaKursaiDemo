package cf.mindaugas.sdademoklp._2_3_generics;

public class Main {
    public static void main(String[] args) {
        // 0. Unbounded box
        // Box<String> boxOfStrings = new Box<>("Stringas 1");
        // Box<Person> boxOfPeople = new Box<>(new Person("F", "L", 15));
        // System.out.println(boxOfPeople.getItem());

        // 1. Upper bounded box
        Box<Person> boxOfPeople = new Box<>(new Person("F", "L", 15));
        Box<ChildOfPerson> boxOfChildren = new Box<>(new ChildOfPerson("F1", "L2", 5));
        // Box<String> boxOfPeople = new Box<>(new Person("F", "L", 15)); // Error !
    }
}

// // unbounded Box - T can be any type
// class Box<T> {
//     private T item;
//
//     public Box(T item) {
//         this.item = item;
//     }
//     public void setItem(T item) {
//         this.item = item;
//     }
//     public T getItem() {
//         return item;
//     }
// }

// upper bounded box - T can be only of type Person or any subclass
class Box<T extends Person> {
    private T item;

    public Box(T item) {
        this.item = item;
    }
    public void setItem(T item) {
        this.item = item;
    }
    public T getItem() {
        return item;
    }

    public int calculate(){
        return this.item.getAge() * 5;
    }
}

class Person {
    private String firstName;
    private String lastName;
    private int age;

    public Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public int getAge(){
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}

class ChildOfPerson extends Person {
    public ChildOfPerson(String firstName, String lastName, int age) {
        super(firstName, lastName, age);
    }
}
