package cf.mindaugas.sdademoklp._2_3_generics;


public class DynamicGenericArraySimplerDemo {
    public static void main(String[] args) {
        DynamicGenericArraySimpler<String> sda = new DynamicGenericArraySimpler<>(5);
        sda.add("Jonas");
        sda.add("Petras");
        System.out.println(sda.getAt(1));

        DynamicGenericArraySimpler<Integer> sda2 = new DynamicGenericArraySimpler<>(5);
        sda2.add(5);
        sda2.add(10);
        System.out.println(sda2.getAt(1));
    }
}

class DynamicGenericArraySimpler<T> {
    private int currentIdx;
    private Object[] array;

    public DynamicGenericArraySimpler(int initialSize){
        array = new Object[initialSize];
    }

    public void add(T c){
        // create a new array and assign to the name of the old one
        if(currentIdx == array.length){
            Object[] tmpArray = new Object[array.length * 2];
            for(int i = 0; i < array.length; i++)
                tmpArray[i] = array[i];
            array = tmpArray;
            tmpArray = null;
        }
        array[currentIdx++] = c; // ... then add an element
    }

    public Object getAt(int idx){
        return array[idx];
    }

    public int length(){
        return currentIdx;
    }
}
