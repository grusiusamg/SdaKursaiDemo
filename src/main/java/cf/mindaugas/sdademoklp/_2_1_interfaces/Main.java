package cf.mindaugas.sdademoklp._2_1_interfaces;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5, 5);

        // we can call the default method
        // .. we did not need to override it
        rectangle.print();
    }
}


interface Shape {
    // public and abstract by default
    double getArea();
    double getPerimeter();
    default void print() {
        System.out.println("Shape: " + this);
    }
}

class Rectangle implements Shape {
    private double a;
    private double b;

    public Rectangle(int i, int i1) {
        this.a = i;
        this.b = i1;
    }

    @Override
    public double getArea() {
        return a * b;
    }

    @Override
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
