package cf.mindaugas.sdademoklp._2_1_interfaces;

import java.util.Arrays;
import java.util.Comparator;

public class AdvancedExample {

    // 1st example
    static void bubbleSort(int[] arr) {
        int counter = 0;
        boolean swapped;
        for (int i = 0; i < arr.length; i++) {
            swapped = false;
            for (int j = 1; j < arr.length - i; j++) { //  optimization 1 (arr.length - i)
                if (arr[j - 1] > arr[j]) {
                    // swap
                    int temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    swapped = true;
                }
                counter++;
            }
            if (!swapped) break;
        }
        System.out.println("Counter: " + counter);
    }

    // 2nd example
    static void bubbleSort(Person[] arr) {
        int counter = 0;
        boolean swapped;
        for (int i = 0; i < arr.length; i++) {
            swapped = false;
            for (int j = 1; j < arr.length - i; j++) {
                if (arr[j - 1].age > arr[j].age) {
                    // swap
                    Person temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    swapped = true;
                }
                counter++;
            }
            if(!swapped) break;
        }
        System.out.println("Counter: " + counter);
    }

    // 3rd example
    static void bubbleSort(Comparable[] arr){
        int n = arr.length;
        for (int i = 0; i < n - 1; i++){
            for (int j = 0; j < n - i - 1; j++){
                if (arr[j].compareTo(arr[j+1]) > 0){
                    Comparable temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    static void firstExample(){
        // 1st example
        int[] arr = {1, 2, 3, 7, 5};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    static void secondExample(){
        // 2nd example
        Person[] people = {
                new Person("Mindaugas", "Jonaitis", 15),
                new Person("Petras", "Jonaitis", 2),
                new Person("Jonas", "Jonaitis", 80)
        };

        System.out.println(Arrays.toString(people));
        bubbleSort(people);
        System.out.println(Arrays.toString(people));
    }

    static void thirdExample(){
        // // ... with comparable
        // Human[] humans = {
        //         new Human("Mikas", "Mikaitis", 30),
        //         new Human("Jonas", "Jonaitis", 40),
        //         new Human("Tomas", "Tomaitis", 20)
        // };
        //
        // System.out.println(Arrays.toString(humans));
        // bubbleSort(humans);
        // System.out.println(Arrays.toString(humans));

        ShoppingCartItem[] shoppingCartItems = {
            new ShoppingCartItem(30),
            new ShoppingCartItem(40),
            new ShoppingCartItem(20)
        };

        System.out.println(Arrays.toString(shoppingCartItems));
        bubbleSort(shoppingCartItems);
        System.out.println(Arrays.toString(shoppingCartItems));
    }

    static void fourthExample(){
        ErasmusStudent[] erasmusStudents = {
                new ErasmusStudent("Mikas", "Mikaitis", 30, 6),
                new ErasmusStudent("Jonas", "Jonaitis", 40, 5),
                new ErasmusStudent("Tomas", "Tomaitis", 20, 10),
                new ErasmusStudent("Maksas", "Maksaitis", 30, 5)
        };

        System.out.println(Arrays.toString(erasmusStudents));
        bubbleSort(erasmusStudents);
        System.out.println(Arrays.toString(erasmusStudents));
    }

    public static void main(String[] args) {
        // Uncomment each method to see how the example works
        // firstExample();
        // secondExample();
        // thirdExample();
        fourthExample();
    }
}

class Person {
    String name;
    String lastName;
    int age;
    Person(String name, String lastName, int age){
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" + name  + ":" + age + '}';
    }
}

class Human implements Comparable {
    String name;
    String lastName;
    int age;
    Human(String name, String lastName, int age){
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" + name  + ":" + age + '}';
    }

    @Override
    public int compareTo(Object o) {
        // h1.compareTo(h2) - kvietimas

        // In reality we will use this
        // return Integer.compare(this.age, ((Human) o).age);

        if(this.age > ((Human)o).age){
            return 1;
        } else if(this.age < ((Human)o).age) {
            return -1;
        } else {
            return 0;
        }
    }
}


class ShoppingCartItem implements Comparable {
    int weightInGrams;

    public ShoppingCartItem(int weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    @Override
    public int compareTo(Object o) {
        if(this.weightInGrams > ((ShoppingCartItem)o).weightInGrams){
            return 1;
        } else if(this.weightInGrams < ((ShoppingCartItem)o).weightInGrams){
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "{" + weightInGrams + "}";
    }
}

// Comparison by two fields, not by one
class ErasmusStudent implements Comparable {
    String name;
    String lastName;
    int age;
    int grade;

    ErasmusStudent(String name, String lastName, int age, int grade){
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.grade = grade;
    }

    @Override
    public int compareTo(Object o) {
        if(this.age == ((ErasmusStudent)o).age)
            if(this.grade == ((ErasmusStudent)o).grade)
                return 0;
            else if (this.grade > ((ErasmusStudent)o).grade)
                return 1;
            else
                return -1;
        else if (this.age > ((ErasmusStudent)o).age)
            return 1;
        else
            return -1;
    }

    @Override
    public String toString() {
        return "{" + name + ":" + lastName + ":" +  age + ":" + grade + "}";
    }
}
